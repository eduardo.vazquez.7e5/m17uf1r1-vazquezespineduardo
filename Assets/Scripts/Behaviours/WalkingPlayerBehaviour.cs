using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingPlayerBehaviour : StateMachineBehaviour
{
    private DataPlayer _dataPlayer;
    private Transform _transform;
    private SpriteRenderer _spriteRenderer;
    private Rigidbody2D _rigidbody2D;
    private float _speed;
    private Vector3 _velocitat;

    private static readonly int WantsToJump = Animator.StringToHash("wantsToJump");
    private static readonly int IsStopped = Animator.StringToHash("isStopped");
    private static readonly int IsFallingDown = Animator.StringToHash("isFallingDown");
    private static readonly int IsAlive = Animator.StringToHash("isAlive");

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _dataPlayer = animator.gameObject.GetComponent<DataPlayer>();
        _transform = animator.gameObject.GetComponent<Transform>();
        _spriteRenderer = animator.gameObject.GetComponent<SpriteRenderer>();
        _speed = _dataPlayer.Speed;
        _velocitat = _dataPlayer.velocitat;
        _rigidbody2D = animator.gameObject.GetComponent<Rigidbody2D>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        float xInput = Input.GetAxis("Horizontal");
        Vector3 inputVector = new Vector3(xInput, 0, 0);
        _velocitat = _speed * inputVector;
        if(_velocitat.magnitude > 0)
            _spriteRenderer.flipX = _velocitat.x < 0;
        if (_dataPlayer.whereIsThePlayer != WhereIsThePlayer.IsFallingDown)
        {
            animator.SetBool(IsStopped, _velocitat == Vector3.zero);
            animator.SetBool(IsFallingDown, false);
        }
        else
            animator.SetBool(IsFallingDown, true);
        _transform.position += _velocitat * Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.Space) && _dataPlayer.whereIsThePlayer == WhereIsThePlayer.IsTouchingGround)
            animator.SetTrigger(WantsToJump);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
