using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingPlayerLoop : StateMachineBehaviour
{
    private Rigidbody2D rb;
    private DataPlayer _dataPlayer;

    private static readonly int HasCrashed = Animator.StringToHash("hasCrashed");

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        rb = animator.gameObject.GetComponent<Rigidbody2D>();
        _dataPlayer = animator.gameObject.GetComponent<DataPlayer>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(_dataPlayer.whereIsThePlayer == WhereIsThePlayer.IsTouchingGround)
            animator.SetTrigger(HasCrashed);
        if(rb.velocity.y < 0)
            animator.SetBool("isFallingDown", true);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}