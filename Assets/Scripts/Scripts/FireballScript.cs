using System;
using System.Collections;
using System.Collections.Generic;
using Mono.Cecil.Cil;
using Unity.VisualScripting;
using UnityEngine;

public class FireballScript : MonoBehaviour
{
    private static readonly int Explode = Animator.StringToHash("Explode");
    private Rigidbody2D rb;
    private static readonly int IsAlive = Animator.StringToHash("isAlive");
    private float _explosionStrength = 5f;

    // Start is called before the first frame update
    void Start()
    {
        rb = this.gameObject.GetComponent<Rigidbody2D>();
        UpdateRotation();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateRotation();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Ground"))
        {
            var rb = gameObject.GetComponent<Rigidbody2D>();
            rb.gravityScale = 0;
            rb.velocity = Vector2.zero;
            rb.angularVelocity = 0;
            GetComponent<Animator>().SetTrigger(Explode);
        }

        if (other.CompareTag("Player"))
        {
            if (this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("IdleFireball"))
            {
                other.GetComponent<Animator>().SetBool(IsAlive, false);
            }
            else
            {
                Vector3 dif = other.GetComponent<Transform>().position - GetComponent<Transform>().position;
                dif = dif.normalized + new Vector3(0, 1f, 0);
                other.GetComponent<Rigidbody2D>().AddForce(_explosionStrength * dif, ForceMode2D.Impulse);
            }
        }

        if (other.CompareTag("Finish"))
            GetComponent<Animator>().SetTrigger(Explode);
    }

    void UpdateRotation()
    {
        double zAngle = 0;
        if(rb.velocity != Vector2.zero)
        {
            var velocity = rb.velocity;
            zAngle = 180/Math.PI*Math.Asin(velocity.x / velocity.magnitude);
        }

        rb.transform.eulerAngles = new Vector3(0, 0, (float) zAngle);
    }
}
