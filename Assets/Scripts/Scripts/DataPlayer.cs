using System.Reflection;
using System.Transactions;
using UnityEngine;

public enum WhereIsThePlayer
{
    IsTouchingGround,
    IsBeginningToFall,
    IsFallingDown
}

public enum GrowingUpPower
{
    NormalSize,
    Growing,
    MaxSize,
    Ungrowing
}

public class DataPlayer : MonoBehaviour
{
    public string Name;
    public PlayerClass playerClass;
    public float Height;
    public float Speed;
    public float DistanceTravelled;
    public float JumpStrength;
    public WhereIsThePlayer whereIsThePlayer;
    private float _timeToFallDown = 0.5f;
    private float _timeFalling = 0f;
    
    
    public Vector3 velocitat;
    private static readonly int WantsToJump = Animator.StringToHash("wantsToJump");
    private static readonly int IsAlive = Animator.StringToHash("isAlive");

    public FiniteStateMachine<GrowingUpPower> GiantPower;

    void Start()
    {
        whereIsThePlayer = WhereIsThePlayer.IsFallingDown;
        velocitat = new Vector3(Speed, 0, 0);

        GiantPower = new FiniteStateMachine<GrowingUpPower>(GrowingUpPower.NormalSize);
        SetUpGiantPower();
        GiantPower.Start();
    }
    
    void Update()
    {
        if (whereIsThePlayer == WhereIsThePlayer.IsBeginningToFall)
        {
            _timeFalling += Time.deltaTime;
            if(_timeFalling > _timeToFallDown)
                whereIsThePlayer = WhereIsThePlayer.IsFallingDown;
        }

        GiantPower.Update();
    }

    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Ground"))
            whereIsThePlayer = WhereIsThePlayer.IsTouchingGround;
        if (other.gameObject.CompareTag("Finish") || other.gameObject.CompareTag("FireBall"))
            GetComponent<Animator>().SetBool(IsAlive, false);
    }
    
    public void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            whereIsThePlayer = WhereIsThePlayer.IsBeginningToFall;
            _timeFalling = 0f;
        }
    }

    private void SetUpGiantPower()
    {
        float timeToGrow = 1f;
        float currentTimeToGrow = 0f;
        float maxSize = 2f;

        float timeInMax = 2f;
        float currentTimeInMax = 0f;
        
        GiantPower.SetNode(
            GrowingUpPower.NormalSize,
            () => {},
            () => {}
        );
        
        GiantPower.SetNode(
            GrowingUpPower.Growing,
            () => currentTimeToGrow = 0f,
            () =>
            {
                currentTimeToGrow += Time.deltaTime;
                MakeProportionalSize(currentTimeToGrow, timeToGrow, maxSize);
            },
            () => MakeProportionalSize(1f, 1f, maxSize),
            _ => currentTimeToGrow >= timeToGrow,
            _ => GrowingUpPower.MaxSize
        );
        
        GiantPower.SetNode(
            GrowingUpPower.MaxSize,
            () => currentTimeInMax = 0f,
            () => currentTimeInMax += Time.deltaTime,
            _ => currentTimeInMax >= timeInMax,
            _ => GrowingUpPower.Ungrowing
            );
        
        GiantPower.SetNode(
            GrowingUpPower.Ungrowing,
            () => currentTimeToGrow = timeToGrow,
            () =>
            {
                currentTimeToGrow -= Time.deltaTime;
                MakeProportionalSize(currentTimeToGrow, timeToGrow, maxSize);
            },
            () => MakeProportionalSize(0f, 1f, maxSize),
            _ => currentTimeToGrow <= 0f,
            _ => GrowingUpPower.NormalSize
        );
    }

    private void MakeProportionalSize(float currentTime, float totalTime, float maxSize)
    {
        float scale = 1f + (maxSize-1f)*currentTime / totalTime;
        GetComponent<Transform>().localScale = new Vector3(scale, scale, 1);
    }
}
