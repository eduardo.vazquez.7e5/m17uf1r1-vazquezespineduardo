using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelManager : MonoBehaviour
{
    private int _numberOfFlames;
    public int NumberOfFlames
    {
        get => _numberOfFlames;
        set
        {
            _numberOfFlames = Math.Min(Math.Max(value, 0), 3);
            UpdateNumberOfFlames();
        }
    }

    [SerializeField] 
    private GameObject prefab;
    [SerializeField] 
    private GameObject cooldownBar;

    private List<GameObject> _listOfFlames;
    
    // Start is called before the first frame update
    void Start()
    {
        _listOfFlames = CreateThreeFlames();
        NumberOfFlames = 0;
    }

    private List<GameObject> CreateThreeFlames()
    {
        List<GameObject> result = new List<GameObject>();
        
        var rt = gameObject.GetComponent<RectTransform>();
        var position = rt.position;
        var upleft = Camera.main.ScreenToWorldPoint(position);
        var rect = rt.rect;
        var downright = Camera.main.ScreenToWorldPoint(position + new Vector3(rect.width, -rect.height, 0));
        for (int i = 0; i < 3; i++)
        {
            var clone = Instantiate(prefab);
            clone.GetComponent<Rigidbody2D>().gravityScale = 0;
            result.Add(clone);
            clone.GetComponent<CapsuleCollider2D>().enabled = false;
            clone.GetComponent<Transform>().position = new Vector3(upleft.x + (0.5f + i)/3f * (downright.x - upleft.x), (downright.y+upleft.y)/2 , 0);
            clone.GetComponent<Transform>().localScale = new Vector3(0.2f, 0.2f, 1);
            clone.tag = "Untagged";
        }
        
        return result;
    }

    private void UpdateNumberOfFlames()
    {
        for (int i = 0; i < 3; i++)
        {
            _listOfFlames[i].GetComponent<SpriteRenderer>().enabled = _numberOfFlames > i;
        }
    }
}
