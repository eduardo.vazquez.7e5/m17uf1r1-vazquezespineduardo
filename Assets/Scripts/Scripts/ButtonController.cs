using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour
{
    public void PlayGame1()
    {
        SaveName();
        SceneManager.LoadScene(1);
    }
    public void PlayGame2()
    {
        SaveName();
        SceneManager.LoadScene(2);
    }

    private void SaveName()
    {
        string name = GameObject.Find("InputName").GetComponent<Text>().text;
        PlayerPrefs.SetString("Name", name);
    }
}
