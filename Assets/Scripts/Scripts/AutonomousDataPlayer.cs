using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum IsMovingTowards{
    Left,
    Right,
    Dead
}

public enum ImmunityPower
{
    Normal,
    Immune
}

public class AutonomousDataPlayer : MonoBehaviour
{
    [SerializeField] 
    private float maxVelocity = 5f;
    [SerializeField] 
    private float acceleration = 3f;

    private float _marginToStatic = 0.75f;
    
    private Rigidbody2D _rigidbody2D;
    private Animator _animator;

    private FiniteStateMachine<IsMovingTowards> _movement;
    public FiniteStateMachine<ImmunityPower> ImmunePower;

    private static readonly int IsStatic = Animator.StringToHash("isStatic");
    private static readonly int IsAlive = Animator.StringToHash("isAlive");

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        
        _movement = new FiniteStateMachine<IsMovingTowards>(IsMovingTowards.Right);
        SetUpMovement();
        _movement.Start();

        ImmunePower = new FiniteStateMachine<ImmunityPower>(ImmunityPower.Normal);
        SetUpImmunity();
        ImmunePower.Start();
    }

    // Update is called once per frame
    void Update()
    {
        _movement.Update();
        _animator.SetBool(IsStatic,_rigidbody2D.velocity.magnitude < _marginToStatic);
        
        ImmunePower.Update();
    }

    void SetUpMovement()
    {
        Vector2 movementVector = acceleration * new Vector2(1, 0);
        
        _movement.SetNode(
            IsMovingTowards.Right,
            () => movementVector = acceleration * new Vector2(1, 0),
            () =>
            {
                if(! HasReachedMaximumVelocity(movementVector))
                    _rigidbody2D.AddForce(movementVector);
            },
            _ => !IsItSafeToKeepRunning(IsMovingTowards.Right),
            _ => IsMovingTowards.Left,
            _ => ! _animator.GetBool(IsAlive),
            _ => IsMovingTowards.Dead
            );
        _movement.SetNode(
            IsMovingTowards.Left,
            () => movementVector = -acceleration * new Vector2(1, 0),
            () =>
            {
                if(! HasReachedMaximumVelocity(movementVector))
                    _rigidbody2D.AddForce(movementVector);
            },
            _ => !IsItSafeToKeepRunning(IsMovingTowards.Left),
            _ => IsMovingTowards.Right,
            _ => ! _animator.GetBool(IsAlive),
            _ => IsMovingTowards.Dead
        );
        _movement.SetNode(
            IsMovingTowards.Dead,
            () => {},
            () => {}
            );
    }

    private bool HasReachedMaximumVelocity(Vector2 appliedForce)
    {
        var velocity = _rigidbody2D.velocity;
        return velocity.magnitude >= maxVelocity && Vector2.Dot(velocity, appliedForce) > 0;
    }

    private bool IsItSafeToKeepRunning(IsMovingTowards state)
    {
        Vector2 lookingAtTheGround;
        Vector2 lookingAtTheSky1;
        Vector2 lookingAtTheSky2;
        Vector2 margin;
        switch (state)
        {
            case IsMovingTowards.Left: default:
                lookingAtTheGround = new Vector2(-1, -1);
                lookingAtTheSky1 = new Vector2(-1, 3);
                lookingAtTheSky2 = new Vector2(-1, 1.5f);
                margin = new Vector2(-2, 0);
                break;
            case IsMovingTowards.Right:
                lookingAtTheGround = new Vector2(1, -1);
                lookingAtTheSky1 = new Vector2(1, 3);
                lookingAtTheSky2 = new Vector2(1, 1.5f);
                margin = new Vector2(2, 0);
                break;         
        }
        RaycastHit2D[] hitsGround = Physics2D.RaycastAll(_rigidbody2D.position+margin, lookingAtTheGround, 3f);
        bool isThereGroundInFront = hitsGround.Any(hit => hit.collider.gameObject.CompareTag("Ground"));
        RaycastHit2D[] kitsSky1 = Physics2D.RaycastAll(_rigidbody2D.position+margin, lookingAtTheSky1, 10f);
        bool isThereFireBallInFront1 = kitsSky1.Any(hit => hit.collider.gameObject.CompareTag("FireBall"));
        RaycastHit2D[] kitsSky2 = Physics2D.RaycastAll(_rigidbody2D.position+margin, lookingAtTheSky2, 3f);
        bool isThereFireBallInFront2 = kitsSky2.Any(hit => hit.collider.gameObject.CompareTag("FireBall"));

        bool isThereFireBallInFront = isThereFireBallInFront1 || isThereFireBallInFront2;
        
        return isThereGroundInFront && !isThereFireBallInFront;
    }

    void SetUpImmunity()
    {
        float immuneTime = 3f;
        float currentImmuneTime = 0f;
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        float timeToBlink = 0.15f;
        float currentTimeToBlink = 0f;
        
        ImmunePower.SetNode(ImmunityPower.Normal, () => { }, () => { });

        ImmunePower.SetNode(ImmunityPower.Immune,
            () =>
            {
                currentImmuneTime = 0f;
                gameObject.tag = "Untagged";
                currentTimeToBlink = 0f;
            },
            () =>
            {
                currentImmuneTime += Time.deltaTime;
                currentTimeToBlink += Time.deltaTime;
                if (currentTimeToBlink >= timeToBlink)
                {
                    currentTimeToBlink -= timeToBlink;
                    sr.enabled = !sr.enabled;
                }
            },
            () =>
            {
                gameObject.tag = "Player";
                sr.enabled = true;
            },
            _ => currentImmuneTime > immuneTime,
            _ => ImmunityPower.Normal,
            _ => _movement.GetCurrentState() == IsMovingTowards.Dead,
            _ => ImmunityPower.Normal
            );
    }
    
    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Finish"))
            GetComponent<Animator>().SetBool(IsAlive, false);
    }
}
