using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public enum MushroomState
{
    IsFalling,
    IsStationary,
    IsAboutToDisappear,
    Done
}

public class EvilMushroom : MonoBehaviour
{
    private FiniteStateMachine<MushroomState> _mushroomState;
    private static readonly int Disappear = Animator.StringToHash("Disappear");

    void Start()
    {
        _mushroomState = new FiniteStateMachine<MushroomState>(MushroomState.IsFalling);
        SetUpMushroomState();
        _mushroomState.Start();
    }

    // Update is called once per frame
    void Update()
    {
        _mushroomState.Update();
    }

    private void SetUpMushroomState()
    {
        float fallingSpeed = 1f;
        Transform _transform = GetComponent<Transform>();
        float stationaryTime = 4f;
        float currentStationaryTime = 0f;
        float timeToDisappear = 1.5f;
        float currentTimeToDisappear = 0f;
        float timeBetweenBlink = 0.1f;
        float currentTimeBetweenBlink = 0f;
        
        _mushroomState.SetNode(
            MushroomState.IsFalling,
            () => {},
            () => _transform.position += fallingSpeed * new Vector3(0,-1,0) * Time.deltaTime
        );
        
        _mushroomState.SetNode(
            MushroomState.IsStationary,
            () => currentStationaryTime = 0f,
            () => currentStationaryTime += Time.deltaTime,
            _ => currentStationaryTime >= stationaryTime,
            _ => MushroomState.IsAboutToDisappear
            );

        _mushroomState.SetNode(
            MushroomState.IsAboutToDisappear,
            () => currentTimeToDisappear = 0f,
            () =>
            {
                currentTimeToDisappear += Time.deltaTime;
                Blink(ref currentTimeBetweenBlink, timeBetweenBlink);
            }, 
            () => Destroy(this.gameObject),
            _ => currentTimeToDisappear >= timeToDisappear,
            _ => MushroomState.Done
            );
        
        _mushroomState.SetNode(
            MushroomState.Done,
            () => {},
            () => {}
            );
    }
    
    private void Blink(ref float currentTime, float totalTime)
    {
        currentTime += Time.deltaTime;
        if (currentTime >= totalTime)
        {
            currentTime -= totalTime;
            GetComponent<SpriteRenderer>().enabled = !GetComponent<SpriteRenderer>().enabled;
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Ground"))
        {
            _mushroomState.SetTrigger(MushroomState.IsFalling, MushroomState.IsStationary);
        }
        else if (other.CompareTag("Player"))
        {
            other.GetComponent<DataPlayer>().GiantPower.SetTrigger(GrowingUpPower.NormalSize, GrowingUpPower.Growing);
            GetComponent<Animator>().SetTrigger(Disappear);
            _mushroomState.SetTrigger(MushroomState.IsFalling, MushroomState.Done);
        }
    }
}
