using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game2VictoryScreen : MonoBehaviour
{
    [SerializeField] 
    private GameObject fireball;
    
    void Start()
    {
        int numberOfFireballs = PlayerPrefs.GetInt("FireballCounter");
        GameObject fb = GameObject.Find("FireballCounter");
        fb.GetComponent<Text>().text = "   x " + numberOfFireballs;
        string name = PlayerPrefs.GetString("Name");
        GameObject.Find("Name").GetComponent<Text>().text = name;
        
        
        var rt = fb.GetComponent<RectTransform>();
        var position = rt.position;
        var upleft = Camera.main.ScreenToWorldPoint(position);
        var rect = rt.rect;
        var downright = Camera.main.ScreenToWorldPoint(position + new Vector3(rect.width, -rect.height, 0));

        var clone = Instantiate(fireball);
        clone.GetComponent<Rigidbody2D>().gravityScale = 0;
        clone.GetComponent<CapsuleCollider2D>().enabled = false;
        clone.GetComponent<Transform>().position = new Vector3(upleft.x, (downright.y+upleft.y)/2 , 0);
        clone.GetComponent<Transform>().localScale = new Vector3(0.2f, 0.2f, 1);
        clone.tag = "Untagged";
    }

    public void GoBackToMenu()
    {
        SceneManager.LoadScene(0);
    }
    
    public void PlayAgain()
    {
        SceneManager.LoadScene(2);
    }
}
