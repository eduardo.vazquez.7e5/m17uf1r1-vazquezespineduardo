using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRight : MonoBehaviour
{
    [SerializeField] 
    public float speed = 5f;
    
    private float _leftLimit;
    private float _rightLimit;
    private float _topLimit;
    private float _bottomLimit;

    private float _width;
    private float _margin = 0.25f;

    private Transform _transform;
    void Start()
    {
        var lowerLeft = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
        var upperRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        _leftLimit = lowerLeft.x;
        _rightLimit = upperRight.x;
        _topLimit = upperRight.y;
        _bottomLimit = lowerLeft.y;

        _width = GetComponent<SpriteRenderer>().bounds.size.x;
        _transform = GetComponent<Transform>();
    }

    void Update()
    {
        _transform.position += speed * new Vector3(1, 0, 0) * Time.deltaTime;
        if (_transform.position.x >= _rightLimit + _width / 2 + _margin)
            _transform.position = new Vector3(_leftLimit - _width / 2 - _margin, _transform.position.y, _transform.position.z);
    }
}
