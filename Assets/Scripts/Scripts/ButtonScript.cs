using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonScript : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler
{
    public bool IsHeldDown => _isHeldDown;
    private bool _isHeldDown;

    private bool _justClicked = false;

    public bool JustClicked()
    {
        if (!_justClicked) return false;
        _justClicked = false;
        return true;
    }
    
    public void OnPointerDown(PointerEventData eventData)
    {
        _justClicked = true;
        _isHeldDown = true;
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        _isHeldDown = false;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
    
    }
}
