using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManager2 : Singleton<GameManager2>
{
    [SerializeField] 
    private GameObject fireBallPrefab;
    [SerializeField] 
    private GameObject coolDownBar;
    [SerializeField]
    private float gameTimeDuration = 60f;
    [SerializeField] 
    private float fireBallCooldown = 2f;
    [SerializeField] 
    private GameObject mushroomPrefab;
    [SerializeField] 
    private float timeToSpawnMushroom = 7f;
    private float _currentTimeToSpawnMushroom;
    
    private float _fireBallSpeedScale = 2f;
    private float _fireBallCooldownCurrent = -1f;
    public int _numberOfFireballs;
    private int _fireballCount;
    
    private FiniteStateMachine<GameState> _gameState;
    
    // Start is called before the first frame update
    void Start()
    {
        _currentTimeToSpawnMushroom = 0f;
        _numberOfFireballs = 0;
        _fireballCount = 0;
        
        _fireBallCooldownCurrent = fireBallCooldown;
        
        _gameState = new FiniteStateMachine<GameState>(global::GameState.InProcess);
        SetUpGameState();
        _gameState.Start();
    }

    // Update is called once per frame
    void Update()
    {
        _gameState.Update();
        UpdateCoolDownBar();
    }
    
    private void SetUpGameState()
    {
        float gameTimeCounter = 60f;
        float waitTimeBeforeVictoryScreen = 3f;
        Animator _adventurer = GameObject.Find("Adventurer").GetComponent<Animator>();
        Text timeCounterComponent = GameObject.Find("TimeCounter").GetComponent<Text>();
        Camera camera = Camera.main;

        _gameState.SetNode(
            global::GameState.InProcess,
            () => gameTimeCounter = gameTimeDuration,
            () =>
            {
                gameTimeCounter -= Time.deltaTime;
                ManageUserInput(camera);
                timeCounterComponent.text = GetTimeCounter2String(gameTimeCounter);
                MushroomController();
            },
            _ => gameTimeCounter <= 0,
            _ => global::GameState.Lost,
            _ => !_adventurer.GetBool("isAlive"),
            _ => GameState.Won
        );
        _gameState.SetNode(
            global::GameState.Won,
            () => {},
            () => waitTimeBeforeVictoryScreen -= Time.deltaTime, 
            () =>
            {
                PlayerPrefs.SetInt("FireballCounter", _fireballCount);
                SceneManager.LoadScene(5);
            },
            _ => waitTimeBeforeVictoryScreen <= 0,
            _ => GameState.Done
        );
        _gameState.SetNode(
            global::GameState.Lost,
            () => {},
            () => waitTimeBeforeVictoryScreen -= Time.deltaTime,
            () =>
            {
                PlayerPrefs.SetInt("FireballCounter", _fireballCount);
                SceneManager.LoadScene(6);
            },
            _ => waitTimeBeforeVictoryScreen <= 0,
            _ => GameState.Done
        );
        _gameState.SetNode(
            global::GameState.Done,
            () => {},
            () => {}
        );
    }

    private void ManageUserInput(Camera camera)
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (_numberOfFireballs > 0)
            {
                SpawnFireBall(Input.mousePosition, camera);
                _numberOfFireballs--;
                GameObject.Find("Panel").GetComponent<PanelManager>().NumberOfFlames = _numberOfFireballs;
                if(_numberOfFireballs == 2)
                    _fireBallCooldownCurrent = fireBallCooldown;
            }
        }
    }

    private void SpawnFireBall(Vector3 mousePosition, Camera camera)
    {
        _fireballCount++;
        var clone = Instantiate(fireBallPrefab);
        Rigidbody2D rb = clone.GetComponent<Rigidbody2D>();
        Vector3 mouseInWorld = camera.ScreenToWorldPoint(mousePosition);
        rb.position = new Vector2(mouseInWorld.x , 5);
        rb.velocity = _fireBallSpeedScale * new Vector2(0, -1);
    }

    private void UpdateCoolDownBar()
    {
        if (_numberOfFireballs < 3)
        {
            _fireBallCooldownCurrent -= Time.deltaTime;
            if (_fireBallCooldownCurrent <= 0)
            {
                _numberOfFireballs++;
                GameObject.Find("Panel").GetComponent<PanelManager>().NumberOfFlames = _numberOfFireballs;
                _fireBallCooldownCurrent = fireBallCooldown;
            }
        }
        
        if(_numberOfFireballs == 3) _fireBallCooldownCurrent = -1f;
        
        var rect = coolDownBar.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(100*Math.Max(_fireBallCooldownCurrent/fireBallCooldown, 0), 4);
    }
    
    private string GetTimeCounter2String(float timeCounter)
    {
        int sec = (int) Math.Floor(timeCounter+0.5f);
        int min = sec / 60;
        sec %= 60;
        return AddZero(min) + ":" + AddZero(sec);
    }
    
    private string AddZero(int value)
    {
        if (value < 10) return "0" + value.ToString();
        return value.ToString();
    }
    
    private void MushroomController()
    {
        _currentTimeToSpawnMushroom += Time.deltaTime;
        if (_currentTimeToSpawnMushroom >= timeToSpawnMushroom)
        {
            _currentTimeToSpawnMushroom -= timeToSpawnMushroom;
            SpawnMushroom();
        }
    }

    private void SpawnMushroom()
    {
        float x = Random.Range(-6f, 6f); 
        float y = 5.5f;
        var clone = Instantiate(mushroomPrefab);
        clone.GetComponent<Transform>().position = new Vector3(x, y, 0);
    }
}
