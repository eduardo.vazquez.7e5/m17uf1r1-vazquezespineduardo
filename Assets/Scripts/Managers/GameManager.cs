using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public enum GameState
{
    InProcess,
    Won,
    Lost,
    Done
}

public class GameManager : Singleton<GameManager>
{
    [SerializeField] 
    private GameObject fireBallPrefab;
    [SerializeField]
    private float timeToSpawnBall = 1f;
    private float _timeBallSpawnerCounter;
    private float _fireBallSpeedScale = 3f;
    private int _fireballCount;
    [SerializeField] 
    private GameObject mushroomPrefab;
    [SerializeField] 
    private float timeToSpawnMushroom = 18f;
    private float _currentTimeToSpawnMushroom = 0f;
    [SerializeField]
    private float gameTimeDuration = 60f;
    private float _gameTimeCounter;

    private FiniteStateMachine<GameState> _gameState;
    private static readonly int IsAlive = Animator.StringToHash("isAlive");

    // Start is called before the first frame update
    void Start()
    {
        _fireballCount = 0;
        _currentTimeToSpawnMushroom = 0f;
        _timeBallSpawnerCounter = 0f;
        
        _gameState = new FiniteStateMachine<GameState>(global::GameState.InProcess);
        SetUpGameState();
        _gameState.Start();
    }

    // Update is called once per frame
    void Update()
    {
        _gameState.Update();
    }

    void SpawnFireBall()
    {
        _fireballCount++;
        float type = Random.Range(0f, 1f);
        float x, y, speedx, speedy;
        if (type < 0.2)
        {
            x = -10;
            y = Random.Range(1f, 5f);
            speedx = Random.Range(2f, 3f);
            speedy = Random.Range((float) Math.Max(-10f*speedx*(2.5f+y),-1), -0.2f);
        }        
        else if (type > 0.8)
        {
            x = 10;
            y = Random.Range(1f, 5f);
            speedx = Random.Range(-3f, -2f);
            speedy = Random.Range((float) Math.Max(10f*speedx*(2.5f+y),-1), -0.2f);
        }
        else
        {
            x = Random.Range(-10f, 10f);
            y = 5;
            speedx = Random.Range(0.2f, 1f);
            if (x > 0) speedx = -speedx;
            speedy = Random.Range(0f, -2f);
        }

        var clone = Instantiate(fireBallPrefab);
        Rigidbody2D rb = clone.GetComponent<Rigidbody2D>();
        rb.position = new Vector2(x, y);
        rb.velocity = _fireBallSpeedScale * new Vector2(speedx, speedy);
    }

    private string GetTimeCounter2String(float timeCounter)
    {
        int sec = (int) Math.Floor(timeCounter+0.5f);
        int min = sec / 60;
        sec %= 60;
        return AddZero(min) + ":" + AddZero(sec);
    }

    private string AddZero(int value)
    {
        if (value < 10) return "0" + value.ToString();
        return value.ToString();
    }

    private void SetUpGameState()
    {
        Animator _player = GameObject.Find("Player").GetComponent<Animator>();
        Text timeCounterComponent = GameObject.Find("TimeCounter").GetComponent<Text>();
        float waitTimeBeforeVictoryScreen = 3f;
        
        _gameState.SetNode(
            global::GameState.InProcess,
            () => _gameTimeCounter = gameTimeDuration,
            () =>
            {
                _gameTimeCounter -= Time.deltaTime;
                timeCounterComponent.text = GetTimeCounter2String(_gameTimeCounter);
                FireBallController();
                MushroomController();
            },
            _ => _gameTimeCounter <= 0,
            _ => global::GameState.Won,
            _ => ! _player.GetBool(IsAlive),
            _ => global::GameState.Lost
            );
        _gameState.SetNode(
            global::GameState.Won,
            () => {},
            () => waitTimeBeforeVictoryScreen -= Time.deltaTime, 
            () =>
            {
                PlayerPrefs.SetInt("FireballCounter", _fireballCount);
                SceneManager.LoadScene(3);
            },
            _ => waitTimeBeforeVictoryScreen <= 0,
            _ => GameState.Done
        );
        _gameState.SetNode(
            global::GameState.Lost,
            () => {},
            () => waitTimeBeforeVictoryScreen -= Time.deltaTime,
            () =>
            {
                PlayerPrefs.SetInt("FireballCounter", _fireballCount);
                SceneManager.LoadScene(4);
            },
            _ => waitTimeBeforeVictoryScreen <= 0,
            _ => GameState.Done
        );
        _gameState.SetNode(
            global::GameState.Done,
            () => {},
            () => {}
        );
    }

    private void FireBallController()
    {
        _timeBallSpawnerCounter += Time.deltaTime;
        if (_timeBallSpawnerCounter >= GetTimeToSpawnBall())
        {
            _timeBallSpawnerCounter -= GetTimeToSpawnBall();
            SpawnFireBall();
        }
    }
    
    private float GetTimeToSpawnBall()
    {
        return (0.5f + 1.5f * _gameTimeCounter / gameTimeDuration) * timeToSpawnBall;
    }

    private void MushroomController()
    {
        _currentTimeToSpawnMushroom += Time.deltaTime;
        if (_currentTimeToSpawnMushroom >= timeToSpawnMushroom)
        {
            _currentTimeToSpawnMushroom -= timeToSpawnMushroom;
            SpawnMushroom();
        }
    }

    private void SpawnMushroom()
    {
        float x = Random.Range(-6f, 6f); 
        float y = 5.5f;
        var clone = Instantiate(mushroomPrefab);
        clone.GetComponent<Transform>().position = new Vector3(x, y, 0);
    }
}
